import re

def read_config_file(config_file_path):
    with open(config_file_path, 'r') as file:
        return file.read()

def extract_ssl_info(config_content, partner_name):
    # Exemple de regex pour trouver les informations SSL pour un partenaire donné
    pattern = rf"{partner_name}.*?USERCID\s*=\s*'([^']+)'.*?ROOTCID\s*=\s*\(\s*'([^']+)'(?:,\s*'([^']+)'(?:,\s*'([^']+)'(?:,\s*'([^']+)')?)?)?\s*\)"
    match = re.search(pattern, config_content, re.DOTALL)
    if match:
        ssl_info = {
            'USERCID': match.group(1),
            'ROOTCID': [match.group(i) for i in range(2, 6) if match.group(i)]
        }
        return ssl_info
    return None

def extract_root_cid(config_content, partner_name):
    ssl_info = extract_ssl_info(config_content, partner_name)
    if ssl_info:
        return ssl_info.get('ROOTCID')
    return None
