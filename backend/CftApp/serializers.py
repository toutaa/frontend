from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import User
from rest_framework import serializers
from .models import Files

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        # Generate token
        refresh = self.get_token(self.user)
        # Populate data dictionary with token
        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)
        data['name'] = self.user.get_full_name()
        data['role'] = 'admin' if self.user.is_staff else 'user'
        return data
##############################################
class FilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        fields = ['id','pdf']
        